import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Exercises from '../pages/Exercises';
import ExercisesNew from '../pages/ExercisesNew';
import NotFound from '../pages/NotFound';

export default function App(){
    return(
        <Router>
            <Switch>
                <Route exact path="/exercise" component={ Exercises } />
                <Route exact path="/exercise/new" component={ ExercisesNew } />
                <Route component={ NotFound } />
            </Switch>
        </Router>
    )
}